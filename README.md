**Gitlab with Docker Registry using Docker Compose**

All configuration options can be found here: https://github.com/sameersbn/docker-gitlab#available-configuration-parameters



**Requirements:**

1. Docker Compose and internet access or loaded using `docker import` images.
2. Generate certificates for registry using `gen_certs.sh`
3. Generate certificates for TLS in HTTPS for Gitlab itself (nginx inside gitlab container). If you self-sign or use self created CA -- don't forget to use FQDNs (and IPs if you need to) in SANs and configure CA accordingly so it adds SANs to signed cert (by default it doesn't).

**Running docker as user:**

You might also need some package install so you can run docker as non-root user.
`sudo dnf install -y fuse-overlayfs uidmap`
If uidmap package is not found you can check if tools `newuidmap` and `newgidmap` are already installed.
You need to add user go `docker` group as main group if you want him to run docker rootless.
`usermod -aG docker user`. If you have problems you can use this manual to diagnose problems: https://phoenixnap.com/kb/cannot-connect-to-the-docker-daemon-error

**Important Notes:**

1. You have to add on guest VM `git.labs.lc` and `registry.git.labs.lc` to /etc/hosts alongside localhost to work if you'
re  testing or configure DNS on production.
2. When genereating certificates using `gen_certs.sh` use `registry.git.labs.lc` as Common Name (2 times) or your registry name.
3. If you have error 500 when opening Docker Registry from Gitlab web interface then check certs permissions. +r them.
4. If you encounter "Docker connection error" when accessing Docker Registry from web interface check p.2 for certs' Common Name (CN) to be correct.
5. You need to generate certs for Gitlab itself too to use HTTPS in it. Example via self signed:

**Generation of a self-signed SSL certificate for HTTPS/TLS:**

STEP 1: Create the server private key

`openssl genrsa -out gitlab.key 2048`

STEP 2: Create the certificate signing request (CSR)

`openssl req -new -key gitlab.key -out gitlab.csr`

STEP 3: Sign the certificate using the private key and CSR

`openssl x509 -req -days 3650 -in gitlab.csr -signkey gitlab.key -out gitlab.crt`

Congratulations! You now have a self-signed SSL certificate valid for 10 years.

Don't forget to generate dhparam.pem too or you will get error:
`openssl dhparam -out dhparam.pem 2048`

The error if some of files are missing (one of them or two or all):
`SSL Key, SSL Certificate and DHParam were not found.`


Info how to put them in you can get here:
https://github.com/sameersbn/docker-gitlab#ssl

But you won't be able to connect runners see p.3 from Requirements above. You have to configure your CA correctly. Example of self made CA that saves SANs can be found here: https://stackoverflow.com/questions/21297139/how-do-you-sign-a-certificate-signing-request-with-your-certification-authority


**Runner registration:** 

You might also have to save certificate to file on runner to be able to register it when self-signed or self-generated. Read more here https://docs.gitlab.com/runner/configuration/tls-self-signed.html#supported-options-for-self-signed-certificates-targeting-the-gitlab-server

or here https://stackoverflow.com/questions/44458410/gitlab-ci-runner-ignore-self-signed-certificate


**Backups:**

When using sameersbn/gitlab based docker image containers it has GitLab version like installed from source. It's because gitlab itself and auxiliary apps (postgresql, redis) use separate images (unlinke official gitlab-ce bundle image).
You don't need to backups secrets because they are set up from docker-compose.yml file using environment variables. Secret locations for GitLab: https://docs.gitlab.com/ee/development/application_secrets.html

Secrets are loaded from .env they are 64 bytes hexadecimal secret key (128 chars long):
`GITLAB_SECRETS_DB_KEY_BASE`, `GITLAB_SECRETS_SECRET_KEY_BASE`, `GITLAB_SECRETS_OTP_KEY_BASE`
You may also want to backup `openid_connect_signing_key` from `secrets.yml`.

But if you explicitly need configuration files from container they are here:
```
/home/git/gitlab/config/secrets.yml
/home/git/gitlab/config/gitlab.yml
```

Other GitLab backup commands: https://docs.gitlab.com/ee/raketasks/backup_gitlab.html


Backup command is executed from /home/git/gitlab:
`docker exec -ti gitlab sudo -u git -H bundle exec rake gitlab:backup:create RAILS_ENV=production`

Backup file created in /home/git/data/backups and named like:
`1682335361_2023_04_24_15.8.6_gitlab_backup.tar`

If you have this error on running backup task:
`Errno::ENOENT: No such file or directory @ rb_check_realpath_internal - /home/git/data/shared/ci_secure_files`

Then: create a folder called `/home/git/data/shared/ci_secure_files` and everything works.


**Restore:**

You can restore a backup only to the exact same version and type (CE/EE) of GitLab that you created it on (for example CE 9.1.0).

First, ensure your backup tar file is in the backup directory described in the gitlab.yml configuration. The default is /home/git/gitlab/tmp/backups, and it needs to be owned by the git user. Now, you can begin the backup procedure:
```
# Stop processes that are connected to the database
sudo service gitlab stop

sudo -u git -H GITLAB_ASSUME_YES=1 bundle exec rake gitlab:backup:restore RAILS_ENV=production
sudo service gitlab restart
```

See also:

Verify database values can be decrypted using the current secrets:
https://docs.gitlab.com/ee/administration/raketasks/check.html#verify-database-values-can-be-decrypted-using-the-current-secrets


**Docker Images:**

sameersbn's (github user) Gitlab Docker image is chosen because Docker Registry can be added easily using env vars and other useful config. 

Regarding Postgresql image: sameersbn's image has cluster options. You can use official docker postgresql image but set it's envs correctly because they have different names.
PostgreSQL image is used sameersbn/postgrsql:14 instead of official postgres Docker Image. It can be easily multiplicated to cluster and used to create snaphots and backups.
Docs and Dockerfile: https://github.com/sameersbn/docker-postgresql

Additional ENV vars for sammersbn/postgresql image:
PG_TRUST_LOCALNET=true/false;
REPLICATION_MODE, REPLICATION_HOST, REPLICATION_PORT, REPLICATION_SSLMODE, REPLICATION_USER and REPLICATION_PASS;
USERMAP_UID, USERMAP_GID.

If you use postgresql:15 version you may encounter "permission denied for schema public" on first initialization in gitlab container logs. 
This happens because of this announcement in official release:
"PostgreSQL 15 also revokes the CREATE permission from all users except a database owner from the public (or default) schema."
source: https://www.postgresql.org/about/news/postgresql-15-released-2526/
Maybe migration of existing DB from 14 to 15 version will work.

Gitlab image documentation:
https://github.com/sameersbn/docker-gitlab


**Recommended steps after clean install.**

1. Disable Auto DevOps:
`Admin area -> Settings -> CI/CD -> 0 to Default to Auto DevOps pipeline for all projects`

**Troubleshooting.**
1. If you get error 500 when trying to enter some page or Admin - Settings - Runners page maybe you have not recovered secrets as mentioned earlier in this document in backup section. You can try using this documentations: https://docs.gitlab.com/ee/administration/backup_restore/backup_gitlab.html#when-the-secrets-file-is-lost You may also try removing application_settings like here: https://forum.gitlab.com/t/500-error-when-updating-application-settings/52113

2. If you have problems when disabling Auto DevOps feature: when you untick checkbox and press save it hangs and then falls back to error 500 and in log you see timeout. You can disable it yourself in database.

In gitlab container:

`/home/git/gitlab# sudo -u git -H bundle exec rails dbconsole -e production --database main`

then:
`gitlabhq_production=> update application_settings set auto_devops_enabled=false;`

you can find and change other features in db like this too.


BSD License.

Reset root password:
https://www.programmerall.com/article/632999018/
