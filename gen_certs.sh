#/bin/bash

CERTS_DIR="./certs/"
 
#####echo "Create Signing Key and CSR"
openssl req -nodes -newkey rsa:2048 -keyout $CERTS_DIR\registry-auth.key -out $CERTS_DIR\registry-auth.csr -subj "/CN=git.labs.lc"
#####echo "Self-Sign Certificate"
openssl x509 -in ${CERTS_DIR}registry-auth.csr -out ${CERTS_DIR}registry-auth.crt -req -signkey ${CERTS_DIR}registry-auth.key -days 3650
 
openssl genrsa -out ${CERTS_DIR}rootCA.key 2048
openssl req -x509 -new -nodes -key ${CERTS_DIR}rootCA.key -days 1024 -out ${CERTS_DIR}rootCA.pem
openssl genrsa -out ${CERTS_DIR}registry.key 2048
openssl req -new -key ${CERTS_DIR}registry.key -out ${CERTS_DIR}registry.csr
###### Common Name
####Common Name (eg, YOUR name) []: registry.git.labs.lc
openssl x509 -req -in ${CERTS_DIR}registry.csr -CA ${CERTS_DIR}rootCA.pem -CAkey ${CERTS_DIR}rootCA.key -CAcreateserial -out ${CERTS_DIR}registry.crt -days 500
